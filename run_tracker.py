import subprocess
import json
from os.path import exists
import time
import datetime


RESULT_FILE = "tracker_result.json"
FILTER_FILE = "filter.json"
FILTER = []
TRACKER_DATA = {}
RUN_FREQUENCY_SECONDS = 100


def main():
    print("### Very simple Linux Tracker running... ###")
    loadFile()
    loadFilter()
    while(True):
        result = subprocess.run(['netstat', '-tp'], stdout=subprocess.PIPE)
        processCOmmandResult(result)
        time.sleep(RUN_FREQUENCY_SECONDS)


def loadFile():
    if(exists(RESULT_FILE)):
        with open(RESULT_FILE, "r") as read_file:
            TRACKER_DATA = json.load(read_file)

def loadFilter():
    global FILTER
    if(exists(FILTER_FILE)):
        with open(FILTER_FILE, "r") as read_file:
            FILTER = json.load(read_file)
            print(str(FILTER))

def processCOmmandResult(result):
    print('.')
    lines = str(result.stdout).split('\\n')
    header = 0
    for line in lines:
        header +=1
        if(header <3):
            continue
        processLine(line)
        with open(RESULT_FILE, "w") as write_file:
            json.dump(TRACKER_DATA, write_file)

def processLine(line):
    elts = line.split()
    if (len(elts) > 5 and len(elts[6].split('/')) > 1 ) :
        process =  elts[6].split('/')[1]
        request = elts[4]
        if isFilterOk(process):
            key = process + '-' + request
            TRACKER_DATA[ key ] = { 'process': process, 'request' : request, 'time' : str(datetime.datetime.now())}
    # else :
        # Don't process
        # print(line)

def isFilterOk(process):
    # print(FILTER)
    for elt in FILTER:
        if process.find(elt) > -1 :
            return False
    return True
 

if __name__ == "__main__":
    main()