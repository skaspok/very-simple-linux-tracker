
# Very simple linux tracker

Check, using netstat, if there wasn't any unexpected request from anywhere on a linux server.


## Requirements
Requires netstat, python3 and admin privileges


## Starting

- Edit filter.json to add any string contained in the process name you want to filter

- Run as sudo: `sudo python3 run_tracker.py`

- check generated file tracker_result.json

## Settings 
Some settings at the beginning of the file

Executes `sudo netstat -tp` every _RUN_FREQUENCY_SECONDS_

## Next step

Maybe some parametrable filtering could be nice...